import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { MantineProvider, NormalizeCSS, GlobalStyles } from '@mantine/core';

ReactDOM.render(
  <React.StrictMode>
    <MantineProvider theme={{
      colorScheme: 'dark',
      breakpoints: {
        xs: 0,
        sm: 576,
        md: 768,
        lg: 992,
        xl: 1200,
      },
    }}>
      <NormalizeCSS />
      <GlobalStyles />
      <App />
    </MantineProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
