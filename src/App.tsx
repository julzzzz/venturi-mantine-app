import React from "react";
import { Home } from "./containers";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/" component={Home} exact></Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
