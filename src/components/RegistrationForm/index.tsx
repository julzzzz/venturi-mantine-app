import React, { useState } from "react";
import { useForm } from "@mantine/hooks";
import {
  Button,
  Group,
  Center,
  Divider,
  Title,
  TextInput,
  RadioGroup,
  Radio,
  Paper,
  Grid,
  Col,
  Alert,
  createStyles,
  RADIO_SIZES,
} from "@mantine/core";
import { CustomPasswordInput } from "../../components";
import { DatePicker } from "@mantine/dates";
import {
  EnvelopeClosedIcon,
  CalendarIcon,
  CheckIcon,
  AvatarIcon,
  BackpackIcon,
  GlobeIcon
} from "@modulz/radix-icons";

const inputMarginBot = "25px";

const useStyles = createStyles((theme) => ({
  form: {
    width: "100%",
    height: "calc(100vh - 60px)",
    background: theme.colors.dark[9],
    overflowX: "hidden",
  },
  formTitle: {
    marginBottom: "10px",
  },
  formGrid: {
    padding: "20px",
    background: theme.colors.dark[9],
  },
  formGridCol: {
    padding: "0 5px",
  },
  formGridColContainer: {
    height: "100%",
    background: theme.colors.dark[9],
    padding: "50px 30px",
  },
  formSection: {
    background: theme.colors.dark[9],
  },
  formSubmitButtonContainer: {
    background: theme.colors.dark[9],
  },
  formSubmitButton: {
    marginBottom: "30px",
    width: "87.5%",
  },
  formInput: {
    marginBottom: inputMarginBot,
  },
  alert: {
    position: "fixed",
    bottom: 20,
    right: 20,
    zIndex: 1000,
  }
}));

function RegistrationForm() {
  const [shownAlert, setShownAlert] = useState(false);
  const form = useForm({
    initialValues: {
      firstName: "",
      lastName: "",
      birthDate: new Date(),
      gender: "male",
      address: "",
      username: "",
      email: "",
      phoneNumber: "",
      password: "",
    },

    validationRules: {
      firstName: (value) => value.length > 0,
      lastName: (value) => value.length > 0,
      birthDate: (value) => String(value).length > 0,
      gender: (value) => value.length > 0,
      address: (value) => value.length > 0,
      username: (value) => value.length > 0,
      email: (value) => /^\S+@\S+$/.test(value),
      phoneNumber: (value) => /[0-9]/.test(value),
      password: (value) => value.length > 0,
    },
  });

  const classes = useStyles();

  const triggerAlert = () => {
    setShownAlert(true);

    setTimeout(function () {
      setShownAlert(false);
    }, 5000);
  };

  return (
    <form
      id="registration-form"
      onSubmit={form.onSubmit((values) => {
        console.log("Form Data: ", values);
        form.reset();
        triggerAlert();
      })}
      className={classes.form}
    >
      <Grid
        gutter="xl"
        id="registration-form-grid"
        className={classes.formGrid}
      >
        <Col span={12} sm={12} md={6} className={classes.formGridCol}>
          <Paper
            className={classes.formGridColContainer}
            padding="xl"
            shadow="lg"
            radius={0}
            withBorder
          >
            <Title order={2} className={classes.formTitle}>
              Registration
            </Title>
            <Paper className={classes.formSection}>
              <Divider margins="md" label="Personal Details" />
              <Group grow className={classes.formInput}>
                <TextInput
                  id="firstName"
                  required
                  placeholder="Your first name"
                  label="First name"
                  radius={0}
                  value={form.values.firstName}
                  onChange={(event) =>
                    form.setFieldValue("firstName", event.currentTarget.value)
                  }
                  error={
                    form.errors.firstName && "Please specify your first name"
                  }
                />
                <TextInput
                  id="lastName"
                  required
                  placeholder="Your last name"
                  label="Last name"
                  radius={0}
                  value={form.values.lastName}
                  onChange={(event) =>
                    form.setFieldValue("lastName", event.currentTarget.value)
                  }
                  error={
                    form.errors.lastName && "Please specify your last name"
                  }
                />
              </Group>
              <DatePicker
                id="birthDate"
                placeholder="Your date of birth"
                label="Birthdate"
                radius={0}
                required
                inputFormat="MM/DD/YYYY"
                labelFormat="MM/YYYY"
                clearable={false}
                icon={<CalendarIcon />}
                defaultValue={form.values.birthDate}
                onChange={(date: Date) => form.setFieldValue("birthDate", date)}
                error={form.errors.birthDate && "Please specify your birthdate"}
                className={classes.formInput}
              />
              <RadioGroup
                id="gender"
                required
                label="Gender"
                color="violet"
                value={form.values.gender}
                onChange={(value) => form.setFieldValue("gender", value)}
                error={form.errors.gender && "Please specify your gender"}
                style={{ marginBottom: inputMarginBot }}
              >
                <Radio value="male">Male</Radio>
                <Radio value="female">Female</Radio>
              </RadioGroup>
              <TextInput
                required
                id="address"
                placeholder="Your physical address"
                label="Address"
                radius={0}
                icon={<GlobeIcon />}
                value={form.values.address}
                onChange={(event) =>
                  form.setFieldValue("address", event.currentTarget.value)
                }
                error={form.errors.address && "Please specify your address"}
                className={classes.formInput}
              />
            </Paper>
          </Paper>
        </Col>
        <Col span={12} sm={12} md={6} className={classes.formGridCol}>
          <Paper
            className={classes.formGridColContainer}
            padding="xl"
            shadow="lg"
            radius={0}
            withBorder
          >
            <Divider
              margins="md"
              label="Account Details"
              style={{ marginTop: "50px" }}
            />
            <TextInput
              required
              id="username"
              placeholder="Your preferred username"
              label="Username"
              radius={0}
              icon={<AvatarIcon />}
              value={form.values.username}
              onChange={(event) =>
                form.setFieldValue("username", event.currentTarget.value)
              }
              error={form.errors.username && "Please specify your username"}
              className={classes.formInput}
            />
            <TextInput
              required
              id="email"
              label="Email"
              icon={<EnvelopeClosedIcon />}
              value={form.values.email}
              onChange={(event) =>
                form.setFieldValue("email", event.currentTarget.value)
              }
              error={form.errors.email && "Please specify valid email"}
              className={classes.formInput}
            />
            <TextInput
              required
              id="phoneNumber"
              placeholder="Your phone number"
              label="Phone Number"
              radius={0}
              icon={<BackpackIcon />}
              value={form.values.phoneNumber}
              onChange={(event) =>
                form.setFieldValue("phoneNumber", event.currentTarget.value)
              }
              error={
                form.errors.phoneNumber && "Please specify valid phone number"
              }
              className={classes.formInput}
            />
            <CustomPasswordInput
              required
              id="password"
              value={form.values.password}
              onChange={(event) =>
                form.setFieldValue("password", event.currentTarget.value)
              }
              error={form.errors.password && "Please specify your password"}
              className={classes.formInput}
            />
          </Paper>
        </Col>
      </Grid>

      <Center className={classes.formSubmitButtonContainer}>
        <Button
          type="submit"
          variant="gradient"
          gradient={{ from: "violet", to: "grape" }}
          className={classes.formSubmitButton}
        >
          Submit
        </Button>
      </Center>

      {Object.values(form.errors).includes(false) && shownAlert && (
        <Alert
          icon={<CheckIcon />}
          color="teal"
          title="Successful Registration"
          className={classes.alert}
        >
          Congratulations! You are successfully registered!
        </Alert>
      )}
    </form>
  );
}

export default RegistrationForm;
