import React, { useState } from "react";
import { useForm } from "@mantine/hooks";
import {
  Button,
  Center,
  PasswordInput,
  TextInput,
  Paper,
  Checkbox,
  createStyles,
} from "@mantine/core";
import { EnvelopeClosedIcon } from "@modulz/radix-icons";

const inputMarginBot = "15px";

const useStyles = createStyles((theme) => ({
  formGridColContainer: {
    padding: "40px 30px",
  },
  formSubmitButton: {
    margin: "15px 0",
    width: "100%",
  },
  formInput: {
    marginBottom: inputMarginBot,
  },
}));

function LoginForm() {
  const form = useForm({
    initialValues: {
      email: "",
      password: "",
    },

    validationRules: {
      email: (value) => /^\S+@\S+$/.test(value),
      password: (value) => value.length > 0,
    },
  });

  const classes = useStyles();

  return (
    <form
      id="login-form"
      onSubmit={form.onSubmit((values) => {
        console.log("Form Data: ", values);
        form.reset();
      })}
    >
      <Paper
        className={classes.formGridColContainer}
        padding="xl"
        shadow="lg"
        radius={0}
        withBorder
      >
        <TextInput
          required
          id="email"
          label="Email"
          icon={<EnvelopeClosedIcon />}
          value={form.values.email}
          onChange={(event) =>
            form.setFieldValue("email", event.currentTarget.value)
          }
          error={form.errors.email && "Please specify valid email"}
          className={classes.formInput}
        />
        <PasswordInput placeholder="Password" label="Password" required />
        <Checkbox
          color="violet"
          label="Remember my credentials"
          style={{ marginTop: inputMarginBot }}
        />
      </Paper>

      <Center>
        <Button
          type="submit"
          variant="gradient"
          gradient={{ from: "violet", to: "grape" }}
          className={classes.formSubmitButton}
        >
          Go
        </Button>
      </Center>
    </form>
  );
}

export default LoginForm;
