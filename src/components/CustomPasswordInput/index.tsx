import React, { useState } from "react";
import { CheckIcon, Cross1Icon, LockClosedIcon } from "@modulz/radix-icons";
import {
  PasswordInput,
  Text,
  Popover,
  PasswordInputProps,
} from "@mantine/core";

function PasswordRequirement({
  meets,
  label,
}: {
  meets: boolean;
  label: string;
}) {
  return (
    <Text
      color={meets ? "teal" : "red"}
      style={{ display: "flex", alignItems: "center", marginTop: 7 }}
      size="sm"
    >
      {meets ? <CheckIcon /> : <Cross1Icon />}{" "}
      <span style={{ marginLeft: 10 }}>{label}</span>
    </Text>
  );
}

const requirements = [
  { re: /[0-9]/, label: "Includes numeric characters" },
  { re: /[a-z]/, label: "Includes lowercase letter" },
  { re: /[A-Z]/, label: "Includes uppercase letter" },
  { re: /[$&+,:;=?@#|'<>.^*()%!-]/, label: "Includes special character" },
];

export default function CustomPasswordInput(props: PasswordInputProps) {
  const value = String(props.value);
  const [popoverOpened, setPopoverOpened] = useState(false);
  const checks = requirements.map((requirement, index) => (
    <PasswordRequirement
      key={index}
      label={requirement.label}
      meets={requirement.re.test(value)}
    />
  ));

  return (
    <Popover
      opened={popoverOpened}
      position="bottom"
      placement="start"
      withArrow
      styles={{ popover: { width: "100%" } }}
      noFocusTrap
      transition="pop-top-left"
      onFocusCapture={() => setPopoverOpened(true)}
      onBlurCapture={() => setPopoverOpened(false)}
      target={
        <PasswordInput
          required
          label="Your password"
          placeholder="Your password"
          description="Strong password should include letters in lower and uppercase, at least 1 number, at least 1 special symbol"
          icon={<LockClosedIcon />}
          value={value}
          onChange={props.onChange}
        />
      }
    >
      <PasswordRequirement
        label="Includes at least 6 characters"
        meets={value.length > 5}
      />
      {checks}
    </Popover>
  );
}
