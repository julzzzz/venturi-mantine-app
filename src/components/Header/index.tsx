import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
  Anchor,
  Burger,
  Divider,
  Group,
  Image,
  Modal,
  createStyles,
} from "@mantine/core";
import { LoginForm } from "../../components";
import {
  HomeIcon,
  ReaderIcon,
  MixIcon,
  EnterIcon,
  BackpackIcon,
} from "@modulz/radix-icons";

const useStyles = createStyles((theme) => ({
  header: {
    background: theme.colors.dark[7],
    height: "60px",
    zIndex: 1,
    padding: "5px 10px",
  },
  brand: {},
  navbarMenu: {
    transition: "height cubic-bezier(0.075, 0.82, 0.165, 1) 1s",
    overflowY: "hidden",
  },
  navbarMenuItem: {
    display: "flex",
    flexDirection: "row",
    padding: "20px 15px",
    color: theme.colors.dark[0],
    textDecoration: "none",
    backgroundColor: theme.colors.dark[6],
  },
  navbarMenuItemIcon: {
    width: "20px",
    height: "20px",
  },
  navbarMenuLabel: {
    marginLeft: "15px",
  },
}));

interface Navbar {
  customStyles?: object;
}

function NavbarMenu(props: Navbar) {
  const [loginModelOpened, setLoginModelOpened] = useState(false);
  const classes = useStyles();

  return (
    <>
      <Modal
        opened={loginModelOpened}
        onClose={() => setLoginModelOpened(false)}
        title="Sign in"
      >
        <LoginForm />
      </Modal>
      <div className={classes.navbarMenu} style={{ ...props.customStyles }}>
        <Anchor component={Link} to="/" className={classes.navbarMenuItem}>
          <HomeIcon className={classes.navbarMenuItemIcon} />
          <span className={classes.navbarMenuLabel}>Home</span>
        </Anchor>
        <Anchor component={Link} to="/" className={classes.navbarMenuItem}>
          <MixIcon className={classes.navbarMenuItemIcon} />
          <span className={classes.navbarMenuLabel}>Services</span>
        </Anchor>
        <Anchor component={Link} to="/" className={classes.navbarMenuItem}>
          <ReaderIcon className={classes.navbarMenuItemIcon} />
          <span className={classes.navbarMenuLabel}>About Us</span>
        </Anchor>
        <Anchor component={Link} to="/" className={classes.navbarMenuItem}>
          <BackpackIcon className={classes.navbarMenuItemIcon} />
          <span className={classes.navbarMenuLabel}>Contact</span>
        </Anchor>
        <Divider size="xs" />
        <Anchor
          component={Link}
          to="/"
          className={classes.navbarMenuItem}
          onClick={() => setLoginModelOpened(true)}
        >
          <EnterIcon className={classes.navbarMenuItemIcon} />
          <span className={classes.navbarMenuLabel}>Login</span>
        </Anchor>
      </div>
    </>
  );
}

function Header() {
  const [collapsed, setCollapsed] = useState(false);
  const title = collapsed ? "Close navigation menu" : "Open navigation menu";

  const classes = useStyles();

  return (
    <>
      <header className={classes.header}>
        <Group position="apart" spacing="lg" withGutter>
          <Image
            width={140}
            height={30}
            src="venturi-full-light-transparent.png"
            className={classes.brand}
          />
          <Burger
            opened={collapsed}
            onClick={() => setCollapsed((status) => !status)}
            title={title}
            size="sm"
            color="white"
          />
        </Group>
      </header>
      <NavbarMenu customStyles={{ height: collapsed ? "310px" : "0px" }} />
    </>
  );
}

export default Header;
