import React from "react";
import { Header, RegistrationForm } from "../../components";
import { Center, createStyles } from "@mantine/core";

const useStyles = createStyles((theme) => ({
  content: {
    height: "calc(100vh - 60px)",
  },
}));

function Home() {
  const classes = useStyles();

  return (
    <div>
      <Header />
      <Center className={classes.content}>
        <RegistrationForm />
      </Center>
    </div>
  );
}

export default Home;
